package se.par.amsen.cartoll.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.par.amsen.cartoll.domain.Adress;
import se.par.amsen.cartoll.repository.AdressRepository;
import se.par.amsen.cartoll.service.AdressService;

@Service
public class AdressServiceImpl implements AdressService{

	@Autowired
	private AdressRepository repository;
	
	@Override
	public long createAdress(Adress adress) {
		return repository.createAdress(adress);
	}

	@Override
	public List<Adress> getAdresses() {
		return repository.getAdresses();
	}

	@Override
	public Adress getAdressById(long id) {
		return repository.getAdressById(id);
	}

	@Override
	public long updateAdress(Adress adress) {
		return repository.updateAdress(adress);
	}

	@Override
	public boolean removeAdress(long id) {
		return repository.removeAdressById(id);
	}

	@Override
	public void clearAllAdresses() {
		repository.clearAllAdresses();
	}

}
