package se.par.amsen.cartoll.repository;

import java.util.List;

import se.par.amsen.cartoll.domain.TaxInterval;
import se.par.amsen.cartoll.domain.TollStation;

public interface TaxIntervalRepository {
	public long createTaxInterval(TaxInterval taxInterval);
	public long updateTaxInterval(TaxInterval taxInterval);
	public List<TaxInterval> getTaxIntervals();
	public TaxInterval getTaxIntervalById(long id);
	public boolean removeTaxIntervalById(long id);
	public void clearAllTaxIntervals();
}
