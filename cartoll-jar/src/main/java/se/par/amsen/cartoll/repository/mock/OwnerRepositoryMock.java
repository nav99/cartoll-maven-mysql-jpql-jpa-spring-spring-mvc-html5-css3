package se.par.amsen.cartoll.repository.mock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import se.par.amsen.cartoll.domain.Owner;
import se.par.amsen.cartoll.repository.OwnerRepository;

public class OwnerRepositoryMock extends GenerateUniqueIdMock implements OwnerRepository{

	private Map<Long,Owner> owners;
	
	public OwnerRepositoryMock() {
		owners = new HashMap<Long,Owner>();
	}
	
	@Override
	public List<Owner> getOwners() {
		List<Owner> ownersTemp = new ArrayList<Owner>();
		ownersTemp.addAll(owners.values());
		return ownersTemp;
	}
	
	@Autowired(required=false)
	@Qualifier("ownerMock")
	public void setOwners(ArrayList<Owner> ownerList) {
		Map<Long, Owner> owners = new HashMap<Long, Owner>();
		
		for(Owner owner : ownerList) {
			owners.put(owner.getId(), owner);
		}
		
		this.owners = owners;
	}

	@Override
	public Owner getOwnerById(long id) {
		return owners.get(id);
	}

	@Override
	public long createOwner(Owner owner) {
		owner.setId(generateUniqueId());
		owners.put(owner.getId(), owner);
		return owner.getId();
	}

	@Override
	public long updateOwner(Owner owner) {
		if(owners.containsKey(owner.getId())) {
			owners.put(owner.getId(), owner);
			return owner.getId();
		}
		return -1;
	}

	@Override
	public boolean removeOwnerById(long id) {
		return owners.remove(id) != null ? true : false;
	}

	@Override
	public void clearAllOwners() {
		owners.clear();
		
	}

}
