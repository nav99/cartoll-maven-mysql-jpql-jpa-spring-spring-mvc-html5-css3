package se.par.amsen.cartoll.repository.mock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import se.par.amsen.cartoll.domain.vehicle.Vehicle;
import se.par.amsen.cartoll.repository.VehicleRepository;

public class VehicleRepositoryMock extends GenerateUniqueIdMock implements VehicleRepository{
	
	private Map<Long,Vehicle> vehicles;

	public VehicleRepositoryMock() {
		vehicles = new HashMap<Long,Vehicle>();
	}
	
	@Override
	public long createVehicle(Vehicle vehicle) {
		vehicle.setId(generateUniqueId());
		vehicles.put(vehicle.getId(), vehicle);
		return vehicle.getId();
	}

	@Override
	public List<Vehicle> getVehicles() {
		List<Vehicle> vehiclesTemp = new ArrayList<Vehicle>();
		vehiclesTemp.addAll(vehicles.values());
		return vehiclesTemp;
	}
	
	@Autowired(required=false)
	@Qualifier("vehicleMock")
	public void setVehicles(ArrayList<Vehicle> vehiclesList) {
		Map<Long, Vehicle> vehicles = new HashMap<Long, Vehicle>();
		
		for(Vehicle vehicle : vehiclesList) {
			vehicles.put(vehicle.getId(), vehicle);
		}
		
		this.vehicles = vehicles;
	}

	@Override
	public Vehicle getVehicleById(long id) {
		return vehicles.get(id);
	}

	@Override
	public long updateVehicle(Vehicle vehicle) {
		if(vehicles.containsKey(vehicle.getId())) {
			vehicles.put(vehicle.getId(), vehicle);
			return vehicle.getId();
		}
		return -1;
	}

	@Override
	public boolean removeVehicleById(long id) {
		return vehicles.remove(id) != null ? true : false;
	}

	@Override
	public void clearAllVehicles() {
		vehicles.clear();
	}
}
