package se.par.amsen.cartoll.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.LocalTime;

import se.par.amsen.cartoll.domain.vehicle.Vehicle;

/**
 * Passage holds the data required to register an occurred Passage, a time, 
 * a Vehicle, a TollStation and the tax for the passage.
 * @author Pär
 *
 */
@Entity
@NamedQueries(value={ 
		@NamedQuery(name="getAllPassages", query="SELECT p FROM Passage p ORDER BY tax DESC"),
		@NamedQuery(name="clearAllPassages", query="DELETE FROM Passage"),
		@NamedQuery(name="getPassagesForTollStation", query="SELECT p FROM Passage p WHERE tollStation_id=?1"),
		@NamedQuery(name="getStatisticsByTollStationId", 
		query="SELECT NEW se.par.amsen.cartoll.domain.statistic.TollStationStatistic(p.tollStation.id, count(p), SUM(p.tax)) from Passage p where tollStation_id=?1)"),
		
		@NamedQuery(name="getStatisticsSummaryForAllTollStations", query="SELECT NEW se.par.amsen.cartoll.domain.statistic.SimpleStatistic(count(p), SUM(p.tax)) FROM Passage p)"),
		
		@NamedQuery(name="getStatisticsByVehicleId", query="SELECT NEW se.par.amsen.cartoll.domain.statistic.SimpleStatistic(count(p), SUM(p.tax)) FROM Passage p WHERE vehicle_id=?1)")
		})
public class Passage {	
	
	@Id
	@GeneratedValue
	private long id;
	
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime date;
	
	private int tax;
	
	@ManyToOne(cascade=CascadeType.MERGE, fetch=FetchType.EAGER)
	@JoinColumn(name="vehicle_id")
	private Vehicle vehicle;
	
	@ManyToOne(cascade=CascadeType.MERGE, fetch=FetchType.EAGER)
	@JoinColumn(name="tollStation_id")
	private TollStation tollStation;
	
	public Passage(Vehicle vehicle, int hour, int minute, TollStation tollStation) {
		LocalTime localTime = new LocalTime(hour,minute);
		this.setDate(localTime.toDateTimeToday());
		this.vehicle = vehicle;
		this.tollStation = tollStation;
	}
	
	protected Passage() { }
	
	/**
	 * Format the DateTime of Passage in "Y-MM-dd HH:mm" -format
	 * @return String representing date in "Y-MM-dd HH:mm" -format
	 */
	public String getFormattedTime() {
		return date.toString("Y-MM-dd HH:mm");
	}

	public DateTime getDate() {
		return date;
	}

	public void setDate(DateTime date) {
		this.date = date;
	}

	public int getTax() {
		return tax;
	}

	public void setTax(int tax) {
		this.tax = tax;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public TollStation getTollStation() {
		return tollStation;
	}

	public void setTollStation(TollStation tollStation) {
		this.tollStation = tollStation;
	}
	
	
}
