package se.par.amsen.cartoll.repository.jpa;

import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import se.par.amsen.cartoll.domain.Adress;
import se.par.amsen.cartoll.domain.Owner;
import se.par.amsen.cartoll.repository.OwnerRepository;

public class OwnerRepositoryJpa extends AbstractRepositoryJpa implements OwnerRepository {

	@Transactional
	@Override
	public long createOwner(Owner owner) {
		em.persist(owner);
		return owner.getId();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Owner> getOwners() {
		return em.createNamedQuery("getAllOwners").getResultList();
	}

	@Override
	public Owner getOwnerById(long id) {
		return em.find(Owner.class, id);
	}

	@Transactional
	@Override
	public long updateOwner(Owner owner) {
		return em.merge(owner).getId();
	}

	@Transactional
	@Override
	public boolean removeOwnerById(long id) {
		em.remove(getOwnerById(id));
		return em.find(Owner.class, id) == null;
	}
	
	@Transactional
	@Override
	public void clearAllOwners(){
		em.createNamedQuery("clearAllOwners").executeUpdate();
	}
}
