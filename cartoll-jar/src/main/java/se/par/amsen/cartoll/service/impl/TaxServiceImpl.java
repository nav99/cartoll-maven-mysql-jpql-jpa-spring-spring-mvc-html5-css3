package se.par.amsen.cartoll.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.par.amsen.cartoll.domain.Passage;
import se.par.amsen.cartoll.domain.TaxInterval;
import se.par.amsen.cartoll.domain.vehicle.Vehicle;
import se.par.amsen.cartoll.repository.TaxIntervalRepository;
import se.par.amsen.cartoll.service.TaxIntervalService;

@Service
public class TaxServiceImpl implements TaxIntervalService{
	
	@Autowired
	private TaxIntervalRepository repository;
	
	@Override
	public int calculateTax(Passage passage) {
		Vehicle vehicle = passage.getVehicle();
		int hour = passage.getDate().getHourOfDay();
		int minute = passage.getDate().getMinuteOfHour();
		
		List<TaxInterval> priceIntervals = repository.getTaxIntervals();
		
		for(TaxInterval priceInterval : priceIntervals) {
			if(priceInterval.isInInterval(hour, minute)) {
				return vehicle.calculateTax(priceInterval.getTaxLevel());
			}
		}
		return -1;
	}

	public TaxIntervalRepository getPriceIntervalRepository() {
		return repository;
	}

	public void setPriceIntervalRepository(TaxIntervalRepository repository) {
		this.repository = repository;
	}

	@Override
	public long createTaxInterval(TaxInterval taxInterval) {
		return repository.createTaxInterval(taxInterval);
	}

	@Override
	public long updateTaxInterval(TaxInterval taxInterval) {
		return repository.updateTaxInterval(taxInterval);
	}

	@Override
	public List<TaxInterval> getTaxIntervals() {
		return repository.getTaxIntervals();
	}

	@Override
	public TaxInterval getTaxIntervalById(long id) {
		return repository.getTaxIntervalById(id);
	}

	@Override
	public boolean removeTaxIntervalById(long id) {
		return repository.removeTaxIntervalById(id);
	}

	@Override
	public void clearAllTaxIntervals() {
		repository.clearAllTaxIntervals();
	}

}
