package se.par.amsen.cartoll.domain.vehicle;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import se.par.amsen.cartoll.domain.Owner;
import se.par.amsen.cartoll.domain.TaxLevel;

/**
 * Truck implements tax calculations specific for a Truck, has a weight and a max free weight
 * used for the tax calculations.
 * @author Pär
 *
 */
@Entity
@DiscriminatorValue(value="Truck")
public class Truck extends Vehicle {
	
	public static final int FREE_WEIGHT_IN_KG = 6000;
	public static final int TAX_PER_TON = 10;
	
	private int weightInKg;
	
	public Truck(String regNumber, Owner owner, int weightInKg) {
		super(regNumber, owner);
		this.weightInKg = weightInKg;
	}
	
	public Truck(){ }

	@Override
	public int calculateTax(TaxLevel taxLevel) {
		int tax = getTaxByLevel(taxLevel);
		
		return isOverWeight() ? tax + getOverWeightInKg() * TAX_PER_TON : tax;
	}
	
	private boolean isOverWeight() {
		return getWeightInKg() >= FREE_WEIGHT_IN_KG;
	}
	
	private int getOverWeightInKg() {
		return (weightInKg-FREE_WEIGHT_IN_KG)/1000;
	}
	
	@Override
	public String getDetailedType() {
		return String.format("Truck, weight: %dkg", getWeightInKg());
	}
	
	@Override
	public String getDetailedName() {
		return String.format("Truck | %s | (%dkg)", getRegNumber(), getWeightInKg());
	}

	@Override
	public int getLowTax() {
		return 12;
	}

	@Override
	public int getMediumTax() {
		return 20;
	}

	@Override
	public int getHighTax() {
		return 25;
	}
	
	public int getWeightInKg() {
		return weightInKg;
	}

}
