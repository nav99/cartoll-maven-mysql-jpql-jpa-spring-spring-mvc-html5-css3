package se.par.amsen.cartoll.service;

import java.util.List;

import se.par.amsen.cartoll.domain.TollStation;

public interface TollStationService {
	public long createTollStation(TollStation tollStation);
	public long updateTollStation(TollStation tollStation);
	public List<TollStation> getTollStations();
	public TollStation getTollStationById(long id);
	public boolean removeTollStation(long id);
	public void clearAllTollStations();
}
