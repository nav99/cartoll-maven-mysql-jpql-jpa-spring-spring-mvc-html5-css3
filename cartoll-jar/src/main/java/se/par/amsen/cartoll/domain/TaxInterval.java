package se.par.amsen.cartoll.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * TaxInterval holds a time interval and the tax applied to this interval
 *  used for calculating the tax for a Passage/Vehicle.
 * @author Pär
 *
 */
@Entity
@NamedQueries(value={	@NamedQuery(name="getAllTaxIntervals", query="SELECT t FROM TaxInterval t ORDER BY hourFrom,minuteFrom ASC"),
						@NamedQuery(name="clearAllTaxIntervals", query="DELETE FROM TaxInterval")})
public class TaxInterval {
	
	@Id
	@GeneratedValue
	private long id;

	private TaxLevel taxLevel;
	
	private int hourFrom;
	private int minuteFrom;
	private int hourTo;
	private int minuteTo;
	
	public TaxInterval(int hourFrom, int minuteFrom, int hourTo, int minuteTo, TaxLevel taxLevel) {
		this.taxLevel = taxLevel;
		this.hourFrom = hourFrom;
		this.minuteFrom = minuteFrom;
		this.hourTo = hourTo;
		this.minuteTo = minuteTo;
	}
	
	protected TaxInterval() { }

	/**
	 * Used to check if a time (hour, minute) is within this TaxInterval
	 * @param hour
	 * @param minute
	 * @return true if supplied time is in interval, else false
	 */
	public boolean isInInterval(int hour, int minute) {
		return ((hour == hourFrom && minute >= minuteFrom) || hour > hourFrom) && ((hour == hourTo && minute <= minuteTo) || hour < hourTo);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public TaxLevel getTaxLevel() {
		return taxLevel;
	}

	public void setTaxLevel(TaxLevel taxLevel) {
		this.taxLevel = taxLevel;
	}
	
}
