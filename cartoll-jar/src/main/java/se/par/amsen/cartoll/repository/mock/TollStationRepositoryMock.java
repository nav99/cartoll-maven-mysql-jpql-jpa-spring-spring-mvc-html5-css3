package se.par.amsen.cartoll.repository.mock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import se.par.amsen.cartoll.domain.TollStation;

import se.par.amsen.cartoll.repository.TollStationRepository;

public class TollStationRepositoryMock extends GenerateUniqueIdMock implements TollStationRepository{

	private Map<Long,TollStation> tollStations;
	
	public TollStationRepositoryMock() {
		tollStations = new HashMap<Long,TollStation>();
	}
	
	@Override
	public long createTollStation(TollStation tollStation) {
		tollStation.setId(generateUniqueId());
		tollStations.put(tollStation.getId(), tollStation);
		return tollStation.getId();
	}

	@Override
	public List<TollStation> getTollStations() {
		List<TollStation> tollStationsTemp = new ArrayList<TollStation>();
		tollStationsTemp.addAll(tollStations.values());
		return tollStationsTemp;
	}
	
	@Autowired(required=false)
	@Qualifier("tollStationMock")
	public void setTollStations(ArrayList<TollStation> tollStationList) {
		Map<Long, TollStation> tollStations = new HashMap<Long, TollStation>();
		
		for(TollStation tollStation : tollStationList) {
			tollStations.put(tollStation.getId(), tollStation);
		}
		
		this.tollStations = tollStations;
	}

	@Override
	public TollStation getTollStationById(long id) {
		return tollStations.get(id);
	}

	@Override
	public long updateTollStation(TollStation tollStation) {
		if(tollStations.containsKey(tollStation.getId())) {
			tollStations.put(tollStation.getId(), tollStation);
			return tollStation.getId();
		}
		return -1;
	}

	@Override
	public boolean removeTollStationById(long id) {
		return tollStations.remove(id) != null ? true : false;
	}

	@Override
	public void clearAllTollStations() {
		tollStations.clear();
	}
}
