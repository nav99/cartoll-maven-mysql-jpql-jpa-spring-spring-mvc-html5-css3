package se.par.amsen.cartoll.domain;

/**
 * TaxLevel is a helper Enum used in the tax calculations for a Passage/Vehicle,
 * a Tax can be FREE, LOW, MEDIUM or HIGH.
 * @author Pär
 *
 */
public enum TaxLevel {
	NO_TAX, LOW, MEDIUM, HIGH
}
