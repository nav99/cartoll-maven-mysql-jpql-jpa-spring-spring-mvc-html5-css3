package se.par.amsen.cartoll.repository;

import java.util.List;

import se.par.amsen.cartoll.domain.Owner;

public interface OwnerRepository {
	public long createOwner(Owner owner);
	public List<Owner> getOwners();
	public Owner getOwnerById(long id);
	public long updateOwner(Owner owner);
	public boolean removeOwnerById(long id);
	public void clearAllOwners();
}
