package se.par.amsen.cartoll.domain.statistic;

/**
 * SimpleStatistic is a simple form of statistics wrapper, has a number of passages and tax amount.
 * @author Pär
 *
 */
public class SimpleStatistic {
	private long passages;
	private long tax;
	
	public SimpleStatistic() {
		this.passages = 0;
		this.tax = 0;
	}
	
	public SimpleStatistic(long passages, long tax) {
		this.passages = passages;
		this.tax = tax;
	}
	
	public long getPassages() {
		return passages;
	}
	public void setPassages(long passages) {
		this.passages = passages;
	}
	public long getTax() {
		return tax;
	}
	public void setTax(long tax) {
		this.tax = tax;
	}
	
	
}
