package se.par.amsen.cartoll.service;

import java.util.List;

import se.par.amsen.cartoll.domain.Passage;
import se.par.amsen.cartoll.domain.TaxInterval;

public interface TaxIntervalService {
	
	/**
	 * Calculate tax for a passage.
	 * @param Passage passage
	 * @return The tax amount for a passage
	 */
	public int calculateTax(Passage passage);
	
	public long createTaxInterval(TaxInterval taxInterval);
	public long updateTaxInterval(TaxInterval taxInterval);
	public List<TaxInterval> getTaxIntervals();
	public TaxInterval getTaxIntervalById(long id);
	public boolean removeTaxIntervalById(long id);
	public void clearAllTaxIntervals();
}
