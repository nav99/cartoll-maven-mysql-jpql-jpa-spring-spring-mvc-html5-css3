package se.par.amsen.cartoll.domain.vehicle;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import se.par.amsen.cartoll.domain.Owner;
import se.par.amsen.cartoll.domain.vehicle.Car;

public class CarTest {	
	private final static String REG_NUMBER = "ABC-123";
	private final static Owner OWNER = new Owner(null,null,null,null,null);

	private Car car; 
	
	@Before
	public void setup() {
		car = new Car(REG_NUMBER, OWNER);
	}
	
	@Test
	public void testConstructor() {
		assertEquals("Registration Number", REG_NUMBER, car.getRegNumber());
		assertEquals("Owner", OWNER, car.getOwner());
	}
}
