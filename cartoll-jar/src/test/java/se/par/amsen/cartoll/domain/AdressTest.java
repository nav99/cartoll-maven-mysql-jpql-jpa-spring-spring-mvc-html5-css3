package se.par.amsen.cartoll.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class AdressTest {
	
	private final static String STREET = "Karlfeldtsgatan 16";
	private final static String CITY = "Göteborg";
	private final static String POSTAL_CODE = "412 68";

	private Adress adress; 
	
	@Before
	public void setup() {
		adress = new Adress(STREET, CITY, POSTAL_CODE);
	}
	
	@Test
	public void testConstructor() {
		assertEquals("Street", STREET, adress.getStreet());
		assertEquals("City", CITY, adress.getCity());
		assertEquals("Postal code", POSTAL_CODE, adress.getPostalCode());
	}

}
