package se.par.amsen.cartoll.mvc;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.par.amsen.cartoll.domain.TollStation;
import se.par.amsen.cartoll.domain.statistic.SimpleStatistic;
import se.par.amsen.cartoll.domain.statistic.TollStationStatistic;
import se.par.amsen.cartoll.service.PassageService;
import se.par.amsen.cartoll.service.TollStationService;


@Controller
public class IndexController {

	@Autowired
	TollStationService tollStationService;
	
	@Autowired
	PassageService passageService;
	
	@RequestMapping("/index")
	public ModelAndView index() {
		ModelAndView mav = new ModelAndView("index");
		
		TollStationSummaryBean tollStationSummaryBean = new TollStationSummaryBean();
		List<TollStationBean> tollStationBeans = new ArrayList<TollStationBean>();
		
		List<TollStation> stations = tollStationService.getTollStations();
		
		for(TollStation station : stations) {
			TollStationBean bean = new TollStationBean();
			bean.setStatistic(passageService.getStatisticsByTollStationId(station.getId()));
			bean.setTollStation(station);
			tollStationBeans.add(bean);
		}
		
		tollStationSummaryBean.setStatistic(passageService.getStatisticsSummaryForAllTollStations());
		tollStationSummaryBean.setTollStationBeans(tollStationBeans);
		
		mav.addObject("tollStationsBean", tollStationSummaryBean);
		
		return mav;
	}
	
	public static class TollStationBean {
		TollStation tollStation;
		TollStationStatistic statistic;
		
		public TollStation getTollStation() {
			return tollStation;
		}
		public void setTollStation(TollStation tollStation) {
			this.tollStation = tollStation;
		}
		public TollStationStatistic getStatistic() {
			return statistic;
		}
		public void setStatistic(TollStationStatistic statistic) {
			this.statistic = statistic;
		}
	}
	
	public static class TollStationSummaryBean {

		List<TollStationBean> tollStationBeans;
		SimpleStatistic statistic;
		
		public List<TollStationBean> getTollStationBeans() {
			return tollStationBeans;
		}
		
		public void setTollStationBeans(List<TollStationBean> tollstationBeans) {
			this.tollStationBeans = tollstationBeans;
		}
		
		public SimpleStatistic getStatistic() {
			return statistic;
		}
		
		public void setStatistic(SimpleStatistic statistic) {
			this.statistic = statistic;
		}
		
	}
}
