package se.par.amsen.cartoll.mvc;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import se.par.amsen.cartoll.domain.Passage;
import se.par.amsen.cartoll.domain.TollStation;
import se.par.amsen.cartoll.domain.statistic.SimpleStatistic;
import se.par.amsen.cartoll.domain.vehicle.Vehicle;
import se.par.amsen.cartoll.service.PassageService;
import se.par.amsen.cartoll.service.TollStationService;
import se.par.amsen.cartoll.service.VehicleService;

@Controller
@RequestMapping(value={"viewPassage", "addPassage", "clearPassages"})
public class PassageController {
	
	@Autowired
	PassageService passageService;
	
	@Autowired
	TollStationService tollStationService;
	
	@Autowired
	VehicleService vehicleService;
	
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView view(@RequestParam(value="id", defaultValue="0") String unparsedId, 
			@RequestParam(value="clear", defaultValue="0") String clear) {
		
		//check if a clear passages param has been supplied
		if(clear.equals("true")) {
			return clearPassages();
		}
		
		int id;
		
		try {
			id = Integer.valueOf(unparsedId);
		} catch(NumberFormatException e) {
			throw new NumberFormatException("Illegal ID parameter");
		}
		
		ModelAndView mav = new ModelAndView("addPassage");
		
		boolean addNewPassage = id <= 0;
		
		if(addNewPassage) {
			List<SimpleBean> tollStationBeans = new ArrayList<SimpleBean>();
			List<SimpleBean> vehicleBeans = new ArrayList<SimpleBean>();
			
			List<TollStation> tollStations = tollStationService.getTollStations();
			List<Vehicle> vehicles = vehicleService.getVehicles();
			
			for(TollStation tollStation : tollStations) {
				tollStationBeans.add(new SimpleBean(tollStation.getId(), tollStation.getName()));
			}
			
			for(Vehicle vehicle : vehicles) {
				vehicleBeans.add(new SimpleBean(vehicle.getId(), vehicle.getDetailedName()));
			}
			
			mav.addObject("passageBean", new PassageBean());
			mav.addObject("tollStationBeans", tollStationBeans);
			mav.addObject("vehicleBeans", vehicleBeans);
		} else {
			Passage passage = passageService.getPassageByPassageId(id);
			if(passage == null) {
				throw new IllegalArgumentException("Could not retrieve passage");
			}
			
			mav.addObject("passage", passage);
		}
		mav.addObject("addNewPassage", addNewPassage);
		return mav;
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView post(PassageBean bean) {
		if(bean.getVehicleId() > 0 && bean.getTollStationId() > 0 && (bean.getHour() < 24 && bean.getHour() >= 0) &&
				(bean.getMinute() < 60 && bean.getMinute() >= 0)) {
			
			Vehicle vehicle = vehicleService.getVehicleById(bean.getVehicleId());
			int hour = bean.getHour();
			int minute = bean.getMinute();
			TollStation tollStation = tollStationService.getTollStationById(bean.getTollStationId());
			
			Passage passage = new Passage(vehicle, hour, minute, tollStation);	
			
			passageService.createPassageAndCalculateTax(passage);
			
			return new ModelAndView("redirect:/viewPassage.html?id=" + passage.getId());
		}
		
		throw new IllegalArgumentException("Error validating supplied passage details");
	}

	public ModelAndView clearPassages() {
		passageService.clearAllPassages();
		return new ModelAndView("redirect:/index.html");
	}
	
	public static class PassageBean {
		private long vehicleId;
		private long tollStationId;
		private int hour;
		private int minute;
		
		public PassageBean() {
			
		}
		
		public long getVehicleId() {
			return vehicleId;
		}
		public void setVehicleId(long vehicleId) {
			this.vehicleId = vehicleId;
		}
		public long getTollStationId() {
			return tollStationId;
		}
		public void setTollStationId(long tollStationId) {
			this.tollStationId = tollStationId;
		}
		public int getHour() {
			return hour;
		}
		public void setHour(int hour) {
			this.hour = hour;
		}
		public int getMinute() {
			return minute;
		}
		public void setMinute(int minute) {
			this.minute = minute;
		}
	}

	public static class SimpleBean {
		long id;
		String name;
		
		public SimpleBean() {
			
		}
		
		public SimpleBean(long id, String name) {
			this.id = id;
			this.name = name;
		}
		
		public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
	}
}
