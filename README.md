# Domain Driven System architecture using Spring #
 *School Project using Spring, JPA, MySQL, SQL, Maven, EasyMock and other technologies*

## Description

This is an examination project made to prove my skills in and understanding of basic system engineering and architecture. I have focused on the back-end functionality and testing.

### Project demo:
![](https://bitbucket.org/TriXigT/cartoll-maven-mysql-jpql-jpa-spring-spring-mvc-html5-css3/downloads/preview.gif)

### Logic

This is a closed end system architecture to handle a "car toll". The project is made to prove my understanding of basic OO (Object Oriented) and DDD (Domain Driven Development) system development techniques. 

Furthermore I have used a large set of technologies listed below to handle the system functionalities (to meet the course requirements). 

The system is built in a domain driven fashion using three layers, domain, service and repository. The repository communicates with a MySQL database and I am using JPA for the ORM (Object Relational Mapping). For the web services I use Spring MVC, also for the context management. 
Furthermore the project is unit tested and integration tested using EasyMock and JUnit.

##Technologies

* Spring (MVC, ORM)
* JPA
* Maven
* SQL
* MySQL
* EasyMock