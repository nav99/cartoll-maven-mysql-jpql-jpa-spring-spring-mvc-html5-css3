package se.hc.database.spotify;

import java.util.ArrayList;
import java.util.List;


public class Album {

	private long id;
	private String name;
	private int releaseYear;
	private List<Song> songs = new ArrayList<Song>();
	private Artist artist;

	public Album(int id, String name, int releaseYear) {
		this(id, null, name, releaseYear);
	}

	public Album(int id, Artist artist, String name, int releaseYear) {
		setId(id);
		setArtist(artist);
		setName(name);
		setReleaseYear(releaseYear);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getReleaseYear() {
		return releaseYear;
	}

	public void setReleaseYear(int releaseYear) {
		this.releaseYear = releaseYear;
	}

	public List<Song> getSongs() {
		return songs;
	}

	public void setSongs(List<Song> songs) {
		this.songs = songs;
	}

	public Artist getArtist() {
		return artist;
	}

	public void setArtist(Artist artist) {
		this.artist = artist;
	}

}
